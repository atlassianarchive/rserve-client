# Atlassian's fork of the RServe Java client library
fork of RServe's java-new client (aka REngine/Rserve jars).

fork taken from:

    svn co svn://svn.rforge.net/Rserve/trunk Rserve

On 7 Feb 2013:

    java-new $ svn info
    Path: .
    URL: svn://svn.rforge.net/org/trunk/rosuda/REngine
    Repository Root: svn://svn.rforge.net/org
    Repository UUID: 955e8658-2bef-402f-a597-8b5c18d8b76d
    Revision: 3466
    Node Kind: directory
    Schedule: normal
    Last Changed Author: urbanek
    Last Changed Rev: 3461
    Last Changed Date: 2012-10-04 10:52:52 +1000 (Thu, 04 Oct 2012)

## Reason for fork:
* pom problems under IVY
* connection exceptions do not report causal exception, only message.

Additional info:
The JRI code has been abandoned.

## Deployment
The binary jars and source are available in the Atlassian Public maven repository:

https://maven.atlassian.com/content/groups/public/com/atlassian/rserve/rserve-client/

Details can be found through the web-interface here:

https://maven.atlassian.com/index.html#nexus-search;quick~rserve-client

## Building
This project has been converted to be built with Maven3.

After checking out, creating a jar should be as simple as executing the following:

    mvn clean package

