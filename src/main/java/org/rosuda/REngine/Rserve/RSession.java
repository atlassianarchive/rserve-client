package org.rosuda.REngine.Rserve;

import org.rosuda.REngine.Rserve.protocol.RPacket;
import org.rosuda.REngine.Rserve.protocol.RTalk;

public class RSession implements java.io.Serializable {
  private static final long serialVersionUID = -6566310121904039457L;

  private final String host;
  private final int port;
  private final byte[] key;
  private final int rsrvVersion;

  transient RPacket attachPacket = null; // response on session attach

  RSession(RConnection c, RPacket p) throws RserveException {
    this.host = c.host();
    this.rsrvVersion = c.rsrvVersion();
    byte[] ct = p.getCont();
    if (ct == null || ct.length != 32 + 3 * 4)
      throw new RserveException(c, "Invalid response to session detach request.");
    this.port = RTalk.getInt(ct, 4);
    this.key = new byte[32];
    System.arraycopy(ct, 12, this.key, 0, 32);
  }

  RSession(SerializationProxy s) {
    this.host = s.host;
    this.port = s.port;
    this.key = s.key;
    this.rsrvVersion = s.rsrvVersion;
  }

  /** attach/resume this session */
  public RConnection attach() {
    RConnection c = new RConnection(this);
    attachPacket = c.rtalk().request(-1);
    return c;
  }

  String host() {
    return host;
  }

  int port() {
    return port;
  }

  int rsrvVersion() {
    return rsrvVersion;
  }

  byte[] key() {
    return key;
  }

  Object writeReplace() {
    return new SerializationProxy(this);
  }

  static final class SerializationProxy {
    final String host;
    final int port;
    final byte[] key;
    final int rsrvVersion;

    SerializationProxy(RSession s) {
      this.host = s.host;
      this.port = s.port;
      this.key = s.key;
      this.rsrvVersion = s.rsrvVersion;
    }

    Object readResolve() {
      return new RSession(this);
    }
  }
}
