package org.rosuda.REngine;

/** REXPSymbol represents a symbol in R. */
public final class REXPSymbol extends REXP {
  /** name of the symbol */
  private final String name;

  /** create a new symbol of the given name */
  public REXPSymbol(String name) {
    super();
    this.name = (name == null) ? "" : name;
  }

  @Override public boolean isSymbol() {
    return true;
  }

  /**
   * returns the name of the symbol
   * 
   * @return name of the symbol
   */
  @Override public String asString() {
    return name;
  }

  @Override public String[] asStrings() {
    return new String[] { name };
  }

  @Override public String toString() {
    return getClass().getName() + "[" + name + "]";
  }

  @Override public String toDebugString() {
    return super.toDebugString() + "[" + name + "]";
  }

  @Override public Object asNativeJavaObject() {
    return name;
  }
}
